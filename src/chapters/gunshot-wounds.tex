\section{Gunshot Wounds}

Gunshot wounds are traumatic injuries caused by projectiles shot from firearms.

\subsection{Treatment}

Treatment for gunshot wounds follows principles for the underlying wounds such as hemorrhage, pneumothorax, fracture, and shock.

\begin{itemize}
\tightlist
\item Control bleeding, in particular:
  \begin{itemize}
  \tightlist
  \item Use a tourniquet for extremities
  \item Use a non-vented chest seal for abdominal injuries
  \item Consider use of a vented chest seal for chest injuries
  \end{itemize}
\item Search for and control bleeding of both entry and exit wounds
\item \textbf{Do not} remove bullets or bullet fragments
\item Splint and stabilize fractures
\item Treat for shock
\item Evacuate to advanced medical care
\end{itemize}
