\section{Drug Overdoses}

In the context of riot medicine, drug overdoses refer to accidental overdoses of recreational drugs.

\subsection{General Treatment}

Some treatment principles are general and can be applied to all types of overdoses.

\begin{itemize}
\tightlist
\item Deescalate to protect yourself, bystanders, and the patient
  \begin{itemize}
  \tightlist
  \item Deescalation minimizes police involvement
  \end{itemize}
\item Determine the drug(s) consumed:
  \begin{itemize}
  \tightlist
  \item Interview the patient and bystanders
  \item Search their pockets and bags
  \end{itemize}
\item \textbf{Do not} give the patient food or drink unless the patient appears to be recovering and can eat and drink on their own
\item \textbf{Do not} induce vomiting
\item Place patient in the rescue position
\item Calm bystanders and keep them away from the police
\end{itemize}

\subsection{Alcohol Overdoses}

In this chapter, alcohol refers to ethanol.

\subsubsection{Signs and Symptoms}

\begin{itemize}
\tightlist
\item Mild intoxication:
  \begin{itemize}
  \tightlist
  \item Euphoria and increased sociability
  \item Impaired judgement, difficulty concentrating, impaired fine motor control
  \item Flushed skin
  \end{itemize}
\item Moderate intoxication:
  \begin{itemize}
  \tightlist
  \item Delayed reactions and confusion
  \item Impaired senses and analgesia
  \item Ataxia, dizziness, and vomiting
  \end{itemize}
\item Severe intoxication:
  \begin{itemize}
  \tightlist
  \item Severe ataxia
  \item Periods of unconsciousness
  \item Anterograde amnesia
  \item Decreased heart and respiratory rates
  \item Pupils that do not respond to light and coma
  \end{itemize}
\item Patients may also have hypotension, hypothermia, or hypoglycemia
\end{itemize}

\subsubsection{Treatment}

There is no way to reverse alcohol intoxication, and most common treatments only alleviate the perception of intoxication.

\begin{itemize}
\tightlist
\item Monitor the patient and determine if they need advanced medical care
\item Consider increased overdose risk if the patient has also used cocaine, barbiturates, benzodiazepines, or opioids
\item Avoid letting them ``sleep it off'' as this can lead to death via aspiration or apnea
\end{itemize}

\subsection{Opioid Overdoses}

Opioids are natural and synthetic substances that are primarily used for pain relief and anaesthesia including opium, morphine, codeine, hydrocodone, heroine, and fentanyl.

\begin{itemize}
\tightlist
\item Effect peak:
  \begin{itemize}
  \tightlist
  \item Oral: 30-60 minutes (90 minutes for controlled release)
  \item Nasal/Injection: Near instantaneous peak
  \end{itemize}
\item Effect duration: 1-6 hours
\end{itemize}

\subsubsection{Signs and Symptoms}

\begin{itemize}
\tightlist
\item Euphoria and reduction in anxiety
\item Drowsiness, disorientation, and delirium
\item Flush and warm skin
\item Constricted pupils
\item Analgesia, muscle weakness, and muscle spasms
\item Nausea
\item Decreased blood pressure, respiratory rate, and heart rates
\item Seizures
\end{itemize}

\subsubsection{Treatment}

\begin{itemize}
\tightlist
\item Administer 2mg of naloxone every 2-3 minutes until the patient improves up to a maximum of 10mg:
  \begin{itemize}
  \tightlist
  \item Narcan typically comes in 4mg doses with 50\% bioavailability when administered nasally
  \item May need to re-administer the duration of action of naloxone is shorter than opioids
  \item Watch for signs of withdrawal after administering
  \item Beware of agitation and combativeness as the patient may be on multiple drugs
  \end{itemize}
\item Consider evacuation to advanced medical care for extended monitoring
\end{itemize}

\subsection{Sedative-Hypnotic Overdoses}

Sedatives are substances that have a calming effect, and hypnotics are substances that induce sleep.
They include phenobarbital, diazepam (Valium), ketamine, and GHB.

\subsubsection{Signs and Symptoms}

General signs and symptoms across many sedative-hypnotics include the following:

\begin{itemize}
\tightlist
\item Euphoria and calmness
\item Loss of coordination, slurred speech, muscle weakness, and difficulty concentrating
\item Decreased blood pressure, respiratory rate, and heart rate
\item Loss of consciousness
\end{itemize}

\subsubsection{Treatment}

\begin{itemize}
\tightlist
\item Consider placing the patient in the rescue position and monitor their ABCs
\item Evacuate to advanced medical care
\item Consider administering naloxone if opioids cannot be ruled out
\end{itemize}

\subsection{Stimulant Overdoses}

Stimulant is a broad classification of drugs that increase CNS activity or are found to be invigorating.
Common recreations stimulants methamphetamine, MDMA, and cocaine.

\subsubsection{Signs and Symptoms}

\begin{itemize}
\tightlist
\item Feelings of happiness and elevated energy levels
\item Agitation and paranoia
\item The feeling of bugs on or under the skin
\item Sweating:
  \begin{itemize}
  \tightlist
  \item Possibly accompanied by dehydration
  \end{itemize}
\item Dilated pupils
\item Increased heart rate, blood pressure, and body temperature
\item Psychosis at high doses:
  \begin{itemize}
  \tightlist
  \item Paranoia, feeling of persecution, anxiety, delusions, and hallucinations
  \end{itemize}
\item Acute coronary syndrome (ACS)
\end{itemize}

\subsubsection{Treatment}

\begin{itemize}
\tightlist
\item Consider treating for hyperthermia
\item Consider evacuation for patients with signs of ACS or serotonin syndrome
\end{itemize}

\subsection{Hallucinogen Overdose}

Hallucinogens are not a distinct category.
Classically they include LSD and psilocybin (mushrooms).
Cannabis, MDMA, and ketamine are also hallucinogens.

\subsubsection{Signs and Symptoms}

\begin{itemize}
\tightlist
\item Physical changes:
  \begin{itemize}
  \tightlist
  \item Dry mouth
  \item Mydriasis
  \item Hypothermia or hyperthermia
  \item Sweating
  \item Increased heart rate
  \item Seizures (rarely)
  \end{itemize}
\item Mental changes:
  \begin{itemize}
  \tightlist
  \item Detachment from reality
  \item Auditory and visual hallucinations
  \item Delusions
  \item Apparent confusion and lack of focus
  \end{itemize}
\item ``Bad trip'':
  \begin{itemize}
  \tightlist
  \item Panic, erratic behavior, and hyperventilation
  \item Feeling trapped
  \item Ego death
  \end{itemize}
\end{itemize}

\subsubsection{Treatment}

\begin{itemize}
\tightlist
\item Calm the patient
\item Attempt to alleviate claustrophobia or agoraphobia
\item Remove excess stimuli such as bright lights and loud noises
\item Help the patient ground themselves in reality by giving them an object to focus on
\end{itemize}
