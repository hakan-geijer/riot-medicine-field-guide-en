#!/usr/bin/env python3
import os
import re
import subprocess
import sys
from argparse import ArgumentParser
from os import path

ROOT_DIR = path.dirname(__file__)
IS_TTY = sys.stdout.isatty()

LOG_CHECKS = {
    'There were undefined references': 'Undefined references',
    'LaTeX Warning: Reference `': 'Undefined references',
    'Label(s) may have changed': 'Incorrect labels',
    'LaTeX Warning: Citation `': 'Missing citation',
    'Package biblatex Warning: The following entry could not be found': 'Missing citation',
    "' multiply defined": 'Duplicate label',
    'No file ': 'Missing input file',
}


def colorize(msg: str, color: str, bold: bool = False) -> str:
    if not IS_TTY:
        return msg

    shell_colors = {
        'gray': '30',
        'red': '31',
        'green': '32',
        'yellow': '33',
        'blue': '34',
        'magenta': '35',
        'cyan': '36',
        'white': '37',
        'crimson': '38',
    }
    attrs = [shell_colors[color]]
    if bold:
        attrs.append('1')
    return '\x1b[{}m{}\x1b[0m'.format(';'.join(attrs), msg)


def exit_with_msg(is_success: bool) -> None:
    if is_success:
        print(colorize('Success!', 'green', True))
        sys.exit(0)
    else:
        print(colorize('Errors present. Check output.', 'red', True))
        sys.exit(1)


def all_files(directory: str, filter_func=None):
    for root, _, fs in os.walk(directory):
        for f in fs:
            full_path = path.join(root, f)
            if filter_func:
                if not filter_func(full_path):
                    continue
            yield full_path


def sign_file(gpg_key: str, file_: str) -> None:
    print(colorize(f'Signing {file_}', 'cyan'))
    subprocess.check_call(
        ['gpg2', '--local-user', gpg_key, '--batch', '--quiet', '--detach-sign', file_])


def check_file(file_name: str, checks: dict) -> bool:
    is_success = True

    with open(file_name) as f:
        for line_num, line in enumerate(f, start=1):
            for search, err_msg in checks.items():
                if search in line:

                    msg = colorize(f'{file_name}: {err_msg}\n', 'red', True)
                    msg += colorize(f'Line {line_num}:\n', 'white', True)
                    msg += line
                    print(msg)
                    is_success = False

    return is_success


def check(args) -> None:
    print(colorize(f'Checking file: {args.doc_name}', 'white', True))

    is_success = True
    is_success &= check_file(f'{args.doc_name}.log', LOG_CHECKS)

    exit_with_msg(is_success)


def release(args) -> None:
    print(colorize('Generating signatures.', 'white', True))
    directory = args.directory
    os.chdir(directory)

    files = all_files('.')
    for f in files:
        if f.endswith('.sig') and args.delete:
            os.remove(f)
        if not f.endswith('.sig') and f'{f}.sig' not in files:
            sign_file(args.gpg_key, f)

    print(colorize('Done.', 'green', True))


def arg_parser() -> ArgumentParser:
    parser = ArgumentParser(os.path.basename(__file__), description='Helper for dev tasks')

    subparsers = parser.add_subparsers()
    subparsers.dest = 'command'
    subparsers.required = True

    sub = subparsers.add_parser('check', help='Check the Latex output')
    sub.add_argument('doc_name', help='Latex file name (without extension)')
    sub.set_defaults(func=check)

    sub = subparsers.add_parser('release', help='Create checksums and sign')
    sub.add_argument('directory', help='Directory to the files to sign')
    sub.add_argument('--delete', help='Delete old signatures first', action='store_true')
    sub.add_argument('--gpg-key', help='GPG signing key fingerprint',
                     default='9D43E6D5AF7B683CB8F8D6896F0EF79BCAF06536')
    sub.set_defaults(func=release)

    return parser


def main() -> None:
    args = arg_parser().parse_args()
    try:
        args.func(args)
    except KeyboardInterrupt:
        print('')
        sys.exit(1)
    except BrokenPipeError:
        sys.exit(1)


if __name__ == '__main__':
    main()
