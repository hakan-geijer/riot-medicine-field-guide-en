# Riot Medicine: Field Guide

This is a companion book to [*Riot Medicine*][riot-medicine] that is meant to serve as a quick reference.

[riot-medicine]: https://gitlab.com/hakan-geijer/riot-medicine-en/

## License

This project and its contents are in the public domain (CC0).
